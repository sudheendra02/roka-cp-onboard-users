import React from 'react'
import ReactDOM from 'react-dom'
import {SessionUtils} from 'react-common-utils'
import {
  ChannelPartnerClientConfigurator
} from '../smart-component/node_modules/cp-player-api-sdk'

import {
  MasterDataClientConfigurator
} from '../smart-component/node_modules/mds-player-api-sdk'

import {OnboardUser} from './src'

function pushState(state) {
  console.log('push state called....');
}

let storage = new SessionUtils();


// const prodBaseUrl = 'https://api.rokahub.com'
const devBaseUrl = 'https://dev-api.rokahub.com'
// const devBaseUrl = 'http://localhost:9081'

ChannelPartnerClientConfigurator(devBaseUrl, storage, null, pushState)
MasterDataClientConfigurator(devBaseUrl,storage,null,pushState)
const props = {}


ReactDOM.render(
  <div className="container">
    <div className="flex-xs-1 flex-sm-2">
      <OnboardUser {...props}/>
    </div>
  </div>,
  document.getElementById('app-root')
)
