import React from 'react';

export default class UploadForm extends React.Component {

  componentDidMount() {
    if (Dropzone) {
      this.myDropzone = new Dropzone('#uploadUserForm', {
        autoProcessQueue: false,
        acceptedFiles: '.csv, .xlsx, .xlx',
        addRemoveLinks: true,
        init: function () {
          this.on("addedfile", function () {
            if (this.files.length > 1 && this.files[1] !== null) {
              this.removeFile(this.files[1]);
            }
          });
        }
      });
    }
  }

  render() {

    return (
      <div>
        <div className="section-title">
          <span className="fa fa-upload"></span> Upload File
          <span className="pull-right">
            <a href="/sample_files/add_employees.xlsx"><span
              className="fa fa-download"></span> Download a Sample File</a>
          </span>
        </div>
        <div className="card-section">Allowed Role Name for upload: <span className="highlight">user</span> , <span
          className="highlight">admin</span></div>
        <div className="panel panel-default">
          <div className="form-card postcard">
            <form id="uploadUserForm" action="/file-upload" className="dropzone form-horizontal">
              <div className="fallback">
                <input name="file" type="file" id="file" multiple/>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

}
