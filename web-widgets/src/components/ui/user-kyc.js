import React from 'react'


export default class UserKyc extends React.Component {
  constructor (props) {
    super(props)
  }
  render() {
      const {
        closeAlertHandler,
        alerts, 
        askKycHandler,
        homeHandler,
        cardsDispatchHandler
    } = this.props;
    
    return(
        <div>
        
        <div className="flex-wrap flex-col-xs flex-row-sm roka-section panel-body">
        <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
            <div className="flex-xs-1 bg-white no-margin">
                {
                  alerts.showAlert === true &&
                  <div className={'notification notification--' + alerts.type}>
                    <div className="notification-action">
                        <span className="icon-close" onClick={() => {
                          closeAlertHandler();
                        }}></span>
                    </div>
                    <div className="notification-message">
                      <strong>{alerts.message}</strong>
                    </div>
                  </div>
                }
                <div className="panel-body no-padding">
                    <div className="panel panel-primary">
                        <div className="form-card postcard">
                            <h3>KYC : Get the Users ready to use Roka Cards</h3>
                        </div>
                    </div>
                </div>
                <div className="row">
                <div className="col-xs-12 col-sm-6 pull-left">
                    <button className="btn btn-secondary btn-block" type="submit" onClick={() => cardsDispatchHandler()}>
                    <span className=""> Check the status of cards dispatched here</span>
                    </button>
                </div></div>
                <br/>
                {/* <div className="row">
                <div className="col-xs-12 col-sm-6 pull-left">
                    <button className="btn btn-primary btn-block" type="submit" onClick={() => askKycHandler()}>
                    <span className=""> Ask all users to send Kyc Documents now</span>
                    </button>
                </div>
                </div> */}
                <br/>
                <div className="row">
                <div className="col-xs-12 col-sm-6 pull-left">
                    <button className="btn btn-primary btn-block" type="submit" onClick={() => homeHandler()}>
                    <span className="">Home</span>
                    </button>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>
      )
  }
}