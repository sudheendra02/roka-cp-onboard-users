import React from 'react';
import UploadFormComponent from './UploadForm'


export default class Form extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { 
      batchNameOnChangeHandler,
      batchRemarksOnChangeHandler,
      submitHandler,
      closeAlertHandler,
      giftCardCheckHandler,
      batchRemarks,
      batchName,
      alerts,
      submitFormStatus,
      isGiftCardUsers
    } = this.props;

    
    const buttonProps = {
      className: 'btn btn-primary btn-block',
      type: 'submit',
      onClick: () => {
        submitFormStatus === false && submitHandler();
      }
    }

    
    return (
    <div className="flex-wrap flex-col-xs flex-row-sm roka-section panel-body">
      <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
        <div className="flex-xs-1 bg-white no-margin">
          {
            alerts.showAlert === true &&
            <div className={"notification notification--" + alerts.type}>
              <div className="notification-action">
                  <span className="icon-close" onClick={() => {
                    closeAlertHandler();
                  }}></span>
              </div>
              <div className="notification-message">
                <strong>{alerts.message}</strong>
              </div>
            </div>
          }
          <div className="panel-body no-padding">
            <div className="panel panel-primary">
              <div className="form-card postcard">
                <form className="form-horizontal">
                  <div className="form-group">
                  <div className="row">
                  <h3> Add Users </h3><br/>
                  <span> <b>Note:</b> Please download the sample file and add the employees you want to provide Roka cards. Further submit the file below.</span>
                  </div><br/>
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor="batchName"> Batch Name :</label>
                        <input type="text" id="batchName" value={batchName} placeholder="Batch Name" className="form-control" onChange = {(e) => batchNameOnChangeHandler(e.target.value)}/>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor="batchRemark"> Batch Remark :</label>
                        <input type="text" id="batchRemark" value={batchRemarks} placeholder="Batch Remark" className="form-control" onChange = {(e) => batchRemarksOnChangeHandler(e.target.value)}/>
                    </div>
                  </div>
                    <div className="col-xs-12">
                      <div className="col-xs-6">
                        <label>
                          Are these gift card users?
                        </label>
                      </div>
                      <div className="col-xs-6">
                        <div className="toggle-container flex-center flex-row-xs no-margin">
                        <span id="toggle-switch"
                              className={!isGiftCardUsers ? 'toggle-switch' : 'toggle-switch yes'}>
                          <span className="toggle" onClick={giftCardCheckHandler} style={{fontSize: '13px'}}>
                            {isGiftCardUsers ? 'Yes' : 'No'}
                          </span>
                        </span>
                        </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>
            
            { <UploadFormComponent/> }
            <div className="form-card postcard">
              <div className="form-horizontal">
                <div className="form-group">
                  <div className="col-xs-12 col-sm-4 pull-right">
                    <button {...buttonProps} disabled={submitFormStatus !== false}>
                      { submitFormStatus === false && <span className="icon-check"> </span>}
                      {
                        submitFormStatus === false
                          ?  'Submit '
                          :
                          <span className="btn-process in">
                            <div className="spinner-holder">
                              <div className="bubble bubble1"></div>
                              <div className="bubble bubble2"></div>
                              <div className="bubble bubble3"></div>
                            </div>
                          <span className="icon-check">
                            Submitted&nbsp;
                            <span className="fa fa-inr">
                            </span>
                          </span>
                        </span>
                      }
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div></div>
    )
  }
}
