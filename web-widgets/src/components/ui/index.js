import React from 'react'
import RequestCards from './request-cards'
import UserKyc from './user-kyc'
import Form from './Form'
import {RokaLoader} from '../common'

export default class OnboardUser extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
      const { currentView, loaderStatus } = this.props;
      if(loaderStatus){
          <RokaLoader/>
        }
      switch(currentView){
          case "request-cards":
            return <RequestCards {...this.props}/>
          case "user-kyc":
            return <UserKyc {...this.props}/>
          case "create-user":
            return <Form {...this.props}/>
          default:
            return <Form {...this.props}/>
      }

  }
}
