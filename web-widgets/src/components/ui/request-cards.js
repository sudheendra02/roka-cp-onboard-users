import React from 'react'
import dropDown from 'lodash'

export default class Signup extends React.Component {
  constructor (props) {
    super(props)
  }
  render() {
      const {
        numberOfCardsHandler, 
        closeAlertHandler,
        recepientName, 
        alerts, 
        recepientNameHandler, 
        officeAddressHandler, 
        city,
        stateName,
        stateNameHandler,
        cityNameHandler,
        pinCode,
        pinCodeHandler,
        requestNowHandler,
        cardsNumber,
        availableStates
    } = this.props;

    const states = availableStates.map(stateDetails => {
        return (<option key={stateDetails.id} value={stateDetails.stateName}>{stateDetails.stateName}</option>)
      })

    return(
        <div>
        
        <div className="flex-wrap flex-col-xs flex-row-sm roka-section panel-body">
        <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
            <div className="flex-xs-1 bg-white no-margin">
                {
                alerts.showAlert === true &&
                <div className={'notification notification--' + alerts.type}>
                    <div className="notification-action">
                        <span className="icon-close" onClick={() => {
                        closeAlertHandler();
                        }}></span>
                    </div>
                    <div className="notification-message">
                    <strong>{alerts.message}</strong>
                    </div>
                </div>
                }
                <div className="panel-body no-padding">
                    <div className="panel panel-primary">
                        <div className="form-card postcard">
                            <h3>Get the Roka Cards now !!</h3>
                            <span> Note : Enter the number of Roka cards you need for your employees. Our exicutive will call you with further details.</span>
                        </div>
                    </div>
                    
                    <div className = 'row'>
                    <div className="col-xs-12 col-sm-6">
                        <span>Number of cards you need :</span>
                        <input type="text" id="reqCards" 
                            className="form-control"
                            placeholder = "Enter required Number of cards "
                            value = {cardsNumber}
                            onChange = {(e) => numberOfCardsHandler(e.target.value)}></input>
                        {/* <select className="col-xs-12 col-sm-3" onChange = {(e) => numberOfCardsHandler(e.target.value)}>
                            { dropDown.range(1, 11).map(value => <option key={value} value={value}>{value}</option>) }
                        </select> */}
                    
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <span>Name of recepient :</span>
                        <input type="text" value={recepientName} placeholder="Recepient Name" className="form-control" onChange = {(e) => recepientNameHandler(e.target.value)}/>
                    </div></div>
                    <br/>
                    <div className = 'row'>
                    <div className="col-xs-12 col-sm-6">
                        <span>Office Address :</span>
                        <textarea rows="4" name="comment" className="form-control" placeholder="Enter your office address here..." onChange = {(e)=>{officeAddressHandler(e.target.value)}}>
                        </textarea> 
                    </div></div><br/>
                    <div className = 'row'>
                    <div className="col-xs-12 col-sm-6">
                        <span>State :</span>
                        {availableStates.length ? <select id="state" className="form-control" tabIndex="7" value={stateName}
                                onChange={(e) => stateNameHandler(e.target.value)}>
                            {states}
                        </select> : 
                        <input type="text" value={stateName}
                            placeholder="Select state" 
                            className="form-control" 
                            onChange={(e) => stateNameHandler(e.target.value)}/>}
                    </div></div><br/>
                    <div className = 'row'>
                    <div className="col-xs-12 col-sm-6">
                        <span>City :</span>
                        <input type="text" placeholder="City" className="form-control" value={city} onChange = {(e) => cityNameHandler(e.target.value)}/>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <span>Pin Code :</span>
                        <input type="text" placeholder="Pin Code" className="form-control" value={pinCode} onChange = {(e) => pinCodeHandler(e.target.value)}/>
                    </div></div>
                </div>
                <br/>
                <div className="col-xs-12 col-sm-4 pull-left">
                    <button className="btn btn-primary btn-block" type="submit" onClick={() => requestNowHandler()}>
                    <span className="icon-check"> </span>Request Now
                    </button>
                </div>
            </div>
        </div>
    </div>
    </div>
      )
  }
}