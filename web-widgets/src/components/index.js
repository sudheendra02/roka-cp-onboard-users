import React, {Component} from 'react'
// import Container from '../../../smart-component/src'
import Container from 'roka-cp-onboard-users-container'
import UI from './ui'

export class OnboardUser extends Component{
    render() {
        return (
            <Container {...this.props}>
                <UI/>
            </Container>
        )
    }
}