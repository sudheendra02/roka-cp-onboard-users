import {Actions, raiseAction} from '../actions'
import {USER_KYC,REQUEST_CARDS, SUCCESS} from '../constants'
import {ChannelPartnerClient} from 'cp-player-api-sdk'
import {RokaEventBus} from 'react-common-utils'
import {MasterDataClient} from 'mds-player-api-sdk'

async function initialize(store, payload){
  
  const request = {
    countryCode: 'IN'
  }
  const response = await MasterDataClient.getStates(request)
  if(response && response.isSuccess){
    store.dispatch(raiseAction(Actions.FETCHED_AVAILABLE_STATES, response.response))
  }
  const {sobStatus} = payload;
  switch(sobStatus){
    case 'userNotUploaded':
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, "create-user"))
      break;
   case 'noRequestForCards':
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, REQUEST_CARDS))
      break;
    default:
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, "create-user"))
      break;
  }
}

async function submitAddUser(store){
  const state = store.getState();
  const {batchName, batchRemarks, isGiftCardUsers} = state
  
  if (!(batchName && batchRemarks)){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Please enter all the fields.'
    }));
  }else{
  var formData = new FormData();
      var file = $('#uploadUserForm').get(0).dropzone.getAcceptedFiles()[0];
      if (!file || file === null) {
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
          showAlert: true,
          type: 'error',
          message: 'Please upload a valid file'
        }));
      } else {
        formData.append('file', file);
        formData.append('batchName', batchName);
        formData.append('batchRemarks', batchRemarks);
        formData.append('isGiftCard', isGiftCardUsers);

        store.dispatch(raiseAction(Actions.SUBMIT_FORM_IN_PROGRESS));
        const response = await ChannelPartnerClient.uploadIndividualUsers(formData)
        // const response = {"status":"SUCCESS","batchUploadResponse":{"httpStatus":"OK","errorCode":null,"status":"SUCCESS","errorMessage":null,"data":null},"masterDataUploadresponse":{"httpStatus":"OK","errorCode":null,"status":"SUCCESS","errorMessage":null,"data":null}}
        
          if (response.status && response.status.toLowerCase() === SUCCESS) {
            if(response.masterDataUploadresponse && 
                response.masterDataUploadresponse.status &&
                  response.masterDataUploadresponse.status.toLowerCase() === SUCCESS) {
                 if(response.batchUploadResponse && response.batchUploadResponse.status &&
                    response.batchUploadResponse.status.toLowerCase() === SUCCESS) {
                      Dropzone.forElement("#uploadUserForm").removeAllFiles(true);
                      store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                        showAlert: true,
                        type: 'success',
                        message: 'Users and Teams added successfully. Please ask your users to send their Id proofs for the KYC process.'
                      }));
                      store.dispatch(raiseAction(Actions.SUBMIT_FORM_SUCCESS))
                      
                      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, REQUEST_CARDS))
                    }
                  else if (response.batchUploadResponse.data && response.batchUploadResponse.data.errorCode === 'DUPLICATE_BATCH_NAME') {
                    store.dispatch(raiseAction(Actions.SUBMIT_FORM_FAILURE))
                    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                      showAlert: true,
                      type: 'error',
                      message: 'Batch Name already exists, please give another'
                    }));
                  } else if(response && response.batchUploadResponse.errorCode === 'INVALID_DATA') {
                       store.dispatch(raiseAction(Actions.SUBMIT_FORM_FAILURE))
                       const msg = response.data
                       let fields = []
                       msg && msg[Object.keys(msg)[0]] && msg[Object.keys(msg)[0]].map((list) => {
                         fields.push(list.field)
                       })
                       const rField = fields.join(",")
                       store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                         showAlert: true,
                         type: 'error',
                         message: `Please fill these required fields ${rField} and Try again`
                       }));
                  } else {
                    store.dispatch(raiseAction(Actions.SUBMIT_FORM_FAILURE))
                    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                      showAlert: true,
                      type: 'error',
                      message: 'Something went wrong, please try again after some time'
                    }));
                  }
            } else {
              store.dispatch(raiseAction(Actions.SUBMIT_FORM_FAILURE))
              store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                showAlert: true,
                type: 'error',
                message: 'Something went wrong, please try again after some time'
              }));
            } 
          } else {
            store.dispatch(raiseAction(Actions.SUBMIT_FORM_FAILURE))
            store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
              showAlert: true,
              type: 'error',
              message: 'Something went wrong. Please try again later'
            }));
          }
      }
    }
}

async function requestCards(store){
  const state = store.getState();
  const {stateName, city, recepientName, pinCode, cardsNumber, officeAddress } = state;
  if(!city){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter valid city name.'
    }));
  }else if(!state){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter valid state name.'
    }));
  }else if(!recepientName){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter valid recepient name.'
    }));
  }else if(pinCode.length!==6){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter valid Pin Code.'
    }));
  } else if(!cardsNumber){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter proper number of cards.'
    }));
  } else if(!officeAddress){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Enter valid office address.'
    }));
  } else{
    let request = {
      contactName : recepientName,
      cardsCount : cardsNumber,
      contactAddress: officeAddress,
      city,
      state : stateName,
      pincode : pinCode,
    }
    store.dispatch(raiseAction(Actions.REQUEST_CARDS_PROCESSING))
    const response = await ChannelPartnerClient.requestCards(request);
    // const response = {status:"success"}
    if (response && response.status.toLowerCase() === SUCCESS){
      store.dispatch(raiseAction(Actions.REQUEST_CARDS_SUCCESS))
      store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
        showAlert: true,
        type: 'success',
        message: 'Successfully requested for cards.'
      }));
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, USER_KYC))
    }else{
      store.dispatch(raiseAction(Actions.REQUEST_CARDS_FAILED))
      store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
        showAlert: true,
        type: 'error',
        message: "Something went wrong. Please try again later."
      }));
    }
  }
  
}

async function cardsDispatchStatus(store){
  store.dispatch(raiseAction(Actions.CARDS_DISPATCH_STATUS_PROCESSING))
  const response = await ChannelPartnerClient.cardsDispatch({});
  if (response && response.status && response.status.toLowerCase() === SUCCESS){
    store.dispatch(raiseAction(Actions.CARDS_DISPATCH_STATUS_SUCCESS))
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'success',
      message: response.data[0].dispatchStatus ? response.data[0].dispatchStatus : 'Not available yet.' 
    }));
  }else{
    store.dispatch(raiseAction(Actions.CARDS_DISPATCH_STATUS_FAILED))
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: 'Status not yet available.'
    }));
  }
}

async function askKycHandler(store){
  store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
    showAlert: true,
    type: 'error',
    message: 'Not yet available.'
  }));
}

function homeHandler(){
  RokaEventBus.emit('RouteToLogin');
}

export default store => next => action => {
  next(action)
  switch(action.type){
    case Actions.COMPONENT_INITIALIZING:
      initialize(store, action.payload)
      break;
    case Actions.REQUEST_NOW_HANDLER:
      requestCards(store);
      break;
    case Actions.SUBMIT_ADD_USERS:
      submitAddUser(store);
      break;
    case Actions.CARDS_DISPATCH_STATUS:
      cardsDispatchStatus(store);
      break;
    case Actions.ASK_KYC_HANDLER:
      askKycHandler(store);
      break;
    case Actions.HOME_HANDLER:
      homeHandler();
    default:
      break;
  }
}