import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions'


const {Actions, raiseAction} = actions

class App extends Component {

  constructor() {
    super()
  }
  
  submitHandler = () => {
    const {
      actions: {raiseAction}
    } = this.props
    raiseAction(Actions.SUBMIT_ADD_USERS)
  }

  closeAlertHandler = () => {
    const {
      actions: {raiseAction}
    } = this.props
    raiseAction(Actions.SET_ALERT_MESSAGE, {
      showAlert: false,
      type: '',
      message: ''
    });
  }

  numberOfCardsHandler = (value) =>{
    const { actions:{raiseAction}} = this.props;
    if(/^[0-9]{0,3}$/.test(value)){
      raiseAction(Actions.SET_NUMBER_OF_CARDS, value)
    }
  }
  
  recepientNameHandler = (value) => {
    const { actions:{raiseAction}} = this.props;
    if(/^[a-zA-Z ]{0,30}$/.test(value)){
      raiseAction(Actions.SET_RECEPIENT_NAME, value)
    }
  }
  
  officeAddressHandler = (value) => {
    const { actions:{raiseAction}} = this.props;
      raiseAction(Actions.SET_OFFICE_ADDRESS_NAME, value)
  }
  
  cityNameHandler = (value) => {
      const { actions:{raiseAction}} = this.props;
      if(/^[a-zA-Z ]{0,30}$/.test(value)){
        raiseAction(Actions.SET_CITY_NAME, value)
    }
  }
  
  requestNowHandler = () => {
    const { actions:{raiseAction}} = this.props;
      raiseAction(Actions.REQUEST_NOW_HANDLER)
  }
 
  pinCodeHandler = (value) => {
    const { actions:{raiseAction}} = this.props;
    if(/^[0-9]{0,6}$/.test(value)){
        raiseAction(Actions.SET_PIN_CODE, value)
    }
  }

  batchNameOnChangeHandler = (value) => {
    const { actions:{raiseAction}} = this.props;
    raiseAction(Actions.SET_BATCH_NAME,value)
  }

  batchRemarksOnChangeHandler = (value) => {
    const { actions:{raiseAction}} = this.props;
    raiseAction(Actions.SET_BATCH_REMARKS,value)
  }

  giftCardCheckHandler = () => {
    const {actions:{raiseAction}} = this.props;
    raiseAction(Actions.SET_GIFT_CARD_CHECK)
  }

  cardsDispatchHandler = () => {
    const {actions:{raiseAction}} = this.props;
    raiseAction(Actions.CARDS_DISPATCH_STATUS)
  }
  
  askKycHandler = () => {
    const {actions:{raiseAction}} = this.props;
    raiseAction(Actions.ASK_KYC_HANDLER)
  }

  homeHandler =()=>{
    const{
      actions:{raiseAction}
    } =this.props;
    raiseAction(Actions.HOME_HANDLER)
  }

  stateNameHandler = (value) => {
    const{actions: {raiseAction}} =this.props;
    if(/^[a-zA-Z ]{0,30}$/.test(value)){
    raiseAction(Actions.SET_STATE_NAME,value)
    }
  }

  render() {
    
    return React.cloneElement(this.props.children, {
      ...this.props,
      closeAlertHandler: this.closeAlertHandler,
      numberOfCardsHandler: this.numberOfCardsHandler,
      cityNameHandler: this.cityNameHandler,
      stateNameHandler: this.stateNameHandler,
      officeAddressHandler: this.officeAddressHandler,
      recepientNameHandler: this.recepientNameHandler,
      pinCodeHandler: this.pinCodeHandler,
      requestNowHandler: this.requestNowHandler,
      submitHandler: this.submitHandler,
      batchNameOnChangeHandler: this.batchNameOnChangeHandler,
      batchRemarksOnChangeHandler: this.batchRemarksOnChangeHandler,
      giftCardCheckHandler: this.giftCardCheckHandler,
      askKycHandler: this.askKycHandler,
      cardsDispatchHandler: this.cardsDispatchHandler,
      homeHandler: this.homeHandler
    })
  }
}

function mapStateToProps(state, ownProps) {
  return {
    ...ownProps,
    ...state,
  }
}

function mapDispatchToProps(dispatch, ownprops) {
  return {
    actions: bindActionCreators({
      raiseAction
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);