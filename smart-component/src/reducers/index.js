import { Actions } from '../actions';


function alerts(state = {showAlert: false, type: 'NONE', message: ''}, action) {
    switch (action.type) {
      case Actions.SET_ALERT_MESSAGE:
        return action.payload;
      default:
        return state;
    }
  }

function pinCode(state = '',action){
    switch(action.type){
        case Actions.SET_PIN_CODE:
            return action.payload;
        default:
            return state;
    }
}

function recepientName( state = '', action){
    switch(action.type){
        case Actions.SET_RECEPIENT_NAME:
            return action.payload;
        default:
            return state;
    }
}

function city (state = '', action){
    switch(action.type){
        case Actions.SET_CITY_NAME:
            return action.payload;
        default:
            return state;
    }
}

function stateName (state = '', action){
  switch(action.type){
      case Actions.SET_STATE_NAME:
          return action.payload;
      default:
          return state;
  }
}

function currentView(state = '', action){
    switch (action.type){
        case Actions.SET_CURRENT_VIEW:
            return action.payload
        default:
            return state
    }
}

function submitFormStatus(state = false, action) {
    switch (action.type) {
      case Actions.SUBMIT_FORM_IN_PROGRESS:
        return true
      case Actions.SUBMIT_FORM_SUCCESS:
      case Actions.SUBMIT_FORM_FAILURE:
        return false
      default:
        return state
    }
  }

function cardsNumber(state='10',action){
    switch (action.type){
        case Actions.SET_NUMBER_OF_CARDS:
            return action.payload;
        default:
            return state;
    }
}

function officeAddress(state='', action){
  switch(action.type){
    case Actions.SET_OFFICE_ADDRESS_NAME:
      return action.payload;
    default:
      return state;
  }
}

function batchName(state = 'add_employees', action) {
  switch (action.type) {
    case Actions.SET_BATCH_NAME:
      return action.payload
    default:
      return state
  }
}

function batchRemarks(state = 'add_employees', action) {
  switch (action.type) {
    case Actions.SET_BATCH_REMARKS:
      return action.payload
    default:
      return state
  }
}

function isGiftCardUsers(state = false, action) {
  switch(action.type) {
    case Actions.SET_GIFT_CARD_CHECK:
      return !state
    case Actions.SUBMIT_FORM_SUCCESS:
      return false
    default:
      return state
  }
}

function availableStates(state = [], action) {
  switch (action.type) {
    case Actions.FETCHED_AVAILABLE_STATES:
      return action.payload
    default:
      return state
  }
}

function loaderStatus(state=false,action){
  switch(action.type){
    case Actions.REQUEST_CARDS_PROCESSING:
      return true
    case Actions.REQUEST_CARDS_SUCCESS:
    case Actions.REQUEST_CARDS_FAILED: 
      return false
    default:
      return state
  }
}


const rootReducer = {
  loaderStatus,
  recepientName,
  alerts,
  city,
  stateName,
  availableStates,
  currentView,
  pinCode,
  officeAddress,
  batchName,
  batchRemarks,
  cardsNumber,
  submitFormStatus,
  isGiftCardUsers
}

export default rootReducer
